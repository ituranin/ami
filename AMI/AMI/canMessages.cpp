/*
 * canMessages.cpp
 *
 * Created: 09.06.2019 16:30:52
 *  Author: Igor
 */ 
#include "canMessages.h"

//////////////////////////////////////////////////////////////////////////

canMsg sysLogMsg;

static void zeroMsgData(canMsg * msg)
{
	int i;
	for(i = 0; i < msg->length; i++)
	msg->data[i] = 0;
}

 /*
 *	returns main state (from FS-rules) from control state
 *	example call: getState(asStates.sacState)
 */
 uint8_t getCSState(uint8_t csState) {
	return (0b110000 & csState)>>4;
 }

 /*
 *	returns substate (from FS-rules) from control state
 *	example call: getState(asStates.sacState)
 */
 uint8_t getCSSubstate(uint8_t csState) {
	return (0b1111 & csState);
 }

 /*
 *	returns csState generated from state and substate
 *	example usage: asStates.sacState = getFullCSState(off, 0);
 */
 uint8_t getFullCSState(uint8_t state, uint8_t substate) {
	return ((state & 0b11)<<4) | (substate & 0b1111);
 }

void initSysLog()
{
	sysLogMsg.id = 0x129;
	sysLogMsg.length = 8;
	zeroMsgData(&sysLogMsg);
}

void sendSysLog(uint8_t state, uint8_t substate, uint8_t byte6, uint8_t byte7)
{
	sysLogMsg.data[0] = getFullCSState(state, substate);
	sysLogMsg.data[6] = byte6;
	sysLogMsg.data[7] = byte7;
	
	sendCANMsg(&sysLogMsg);
}