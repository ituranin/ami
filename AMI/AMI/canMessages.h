/*
 * canMessages.h
 *
 * Created: 09.06.2019 16:30:30
 *  Author: Igor
 */ 


#ifndef CANMESSAGES_H_
#define CANMESSAGES_H_

//////////////////////////////////////////////////////////////////////////
//Masks
#define AS_STATE_MASK		0b111			//3 bits
#define AS_SUBSTATE_MASK  0b11111			//5 bits

#define CS_STATE_MASK		 0b11			//2 bits
#define CS_SUBSTATE_MASK   0b1111			//4 bits
#define ASSI_STATE_MASK     0b111           //3 bits


#include "DriversAVR/CAN/CAN.h"
#include "StatusCodeConventions/Definition_ASStates.h"
////////////////////////////////////////////////////////////
// variables

//////////////////////////////////////////////////////////////////////////
 //CS States

 /*
*	returns main state (from FS-rules) from control state
*	example call: getState(asStates.sacState)
*/
uint8_t getCSState(uint8_t csState);

/*
*	returns substate (from FS-rules) from control state
*	example call: getState(asStates.sacState)
*/
uint8_t getCSSubstate(uint8_t csState);


/*
 *	returns csState generated from state and substate
 *	example usage: asStates.sacState = getFullCSState(off, 0);
 */
 uint8_t getFullCSState(uint8_t state, uint8_t substate);

////////////////////////////////////////////////////

void initSysLog();

void sendSysLog(uint8_t state, uint8_t substate, uint8_t byte6, uint8_t byte7);

#endif /* CANMESSAGES_H_ */