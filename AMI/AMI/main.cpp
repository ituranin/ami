/*
 * AMI.cpp
 *
 * Created: 26.04.2018 22:34:15
 * Author : Igor
 */ 


#define LOW_BYTE(x)        (x & 0xff)
#define HIGH_BYTE(x)       ((x >> 8) & 0xff)


#define F_CPU 16000000
#define MISSION_COUNT 10
#define TIMER_TIMEOUT 2
#define PRESSURE_THRESHOLD 100

#define AMI_SUBSTATE_NORMAL         0
#define AMI_SUBSTATE_ENABLE_STARTER 1
#define AMI_SUBSTATE_CLOSE_SC       2

#include <avr/io.h>
#include <util/delay.h>

#include <stdio.h>
#include <stdlib.h>
#include "DriversAVR/CAN/CAN.h"
#include "DriversAVR/LCD/lcdSPI.h"
#include "DriversAVR/Interrupt/interrupt.h"
#include "DriversAVR/Timer/Timer.h"
#include "StatusCodeConventions/Definition_DeviceIDs.h"
#include "StatusCodeConventions/Definition_ControllerStates.h"
#include "StatusCodeConventions/Definition_CANMessages.h"
#include "StatusCodeConventions/Definition_ASMissions.h"
#include "StatusCodeConventions/Definition_ASStates.h"
#include "canMessages.h"
#include "DriversAVR/Button/button.h"

// variables
char* missions[MISSION_COUNT] = {
	"NONE",
	"MANUAL",
	"MAINTENANCE",
	"ACCELERATION",
	"SKIDPAD",
	"AUTOCROSS",
	"TRACKDRIVE",
	"EBS TEST",
	"INSPECTION",
	"CONTROLLER"
};
	
volatile uint8_t mode = AMI_STATE_SHOW_RELEASE;
volatile uint8_t substate = AMI_SUBSTATE_NORMAL;
uint8_t screenInitialized = 0;
volatile uint8_t interruptsSet = 0;
uint8_t asDrivingReceived = 0;
uint8_t updateDisplay = 0;
uint8_t selected = 0;
uint8_t confirmed = 0;
uint8_t propagate = 0;
volatile uint8_t current = MISSION_MANUAL;
volatile uint8_t lockedPosition = MISSION_NONE;
uint8_t colorChangeIndicator = 0;
volatile bool asms = false;
volatile uint8_t asState = 0;
uint8_t syslogCounter = 0;
volatile int8_t gear = 6;

uint8_t byte6 = 0;
uint8_t byte7 = 0;

//messages to receive
canMsg asStateMessage;
canMsg dashOneMessage;
canMsg asscSysLog;
canMsg sbSensorMessage;

uint16_t rearPressure = 10;
uint16_t frontPressure = 10;

buttonInput yellow;
buttonInput white;
buttonInput asmsButton;

volatile bool hasWaitedForPressure = false;
uint8_t waitCounter = 0;
uint8_t displayUpdateCounter = 0;
char tempText[9];

// function prototypes
void sendMissionLocked(uint8_t);
void setUImodeIndicator();
void propagateIfSelected();
void showRestartScreen();
void showConfirmationQuestion();

void buttonModeCycle();
void buttonModeConfirm();
void buttonModeOff();
void cycleHandler();
void selectionHandler();
void yesHandler();
void noHandler();
void asmsPressed();
void asmsReleased();

uint16_t to16bit(uint8_t low, uint8_t high) {
	return low | static_cast<uint16_t>(high << 8);
}

bool checkYellowLevel()
{
	return getExtIntLevel(INT0);
}

bool checkWhiteLevel()
{
	return getExtIntLevel(INT1);
}

bool checkAsmsLevel()
{
	return getExtIntLevel(INT3);
}

void initInts()
{
	initButton(&yellow, checkYellowLevel, 0, 0);
	initButton(&white, checkWhiteLevel, 0, 0);
	initButton(&asmsButton, checkAsmsLevel, asmsPressed, asmsReleased);
}

void asmsPressed()
{
	asms = true;
}

void asmsReleased()
{
	asms = false;
}

void buttonModeCycle()
{
	yellow.btnPressedHandler = cycleHandler;
	white.btnPressedHandler = selectionHandler;
}

void buttonModeConfirm()
{
	yellow.btnPressedHandler = yesHandler;
	white.btnPressedHandler = noHandler;
}

void buttonModeOff()
{
	yellow.btnPressedHandler = 0;
	white.btnPressedHandler = 0;
}

void displayContentPressure(uint16_t* pressure)
{
	utoa(*pressure, tempText, 10);

	if(*pressure >= 1000)
	{
		tempText[3] = tempText[2];
	}
	else if (*pressure >= 100)
	{
		tempText[3] = tempText[1];
		tempText[1] = tempText[0];
		tempText[0] = '0';
	}
	else //set display to 0
	{
		tempText[3] = tempText[0];
		tempText[1] = '0';
		tempText[0] = '0';
	}

	tempText[2] = '.';
	tempText[4] = 'b';
	tempText[5] = 'a';
	tempText[6] = 'r';
	tempText[7] = '\0';

	LCD_string(tempText);
}


// init functions
void updateScreen(){
	
	if (!screenInitialized || updateDisplay)
	{
		updateDisplay = 0;
		LCD_clear();
	
		LCD_cursor(0, 0);
		
		if (mode == AMI_STATE_NORMAL || mode == AMI_STATE_LOCKED)
		{
			if (!selected || confirmed)
			{
				LCD_string("    MISSION     ");
				
				LCD_cursor(1, 0);
				LCD_string(missions[current]);
			} else {
				showConfirmationQuestion();
			}
		}
		
		if (mode == AMI_STATE_SHOW_RELEASE)
		{
			LCD_string("Pressure release");
			LCD_cursor(1, 0);
			displayContentPressure(&rearPressure);
			LCD_cursor(1, 9);
			displayContentPressure(&frontPressure);
		}
		
		if(mode == AMI_STATE_SHOW_RESTART)
		{
			showRestartScreen();
		}
	
		setUImodeIndicator();
		
		screenInitialized = 1;
		
	}
	
}

inline void disStarter()
{	
	PORTB &= ~(1<<PINB0);
}

inline void gearLightOff()
{
	PORTC &= ~(1<<PINC6);
}

inline void enStarter()
{	
	PORTB |= (1<<PINB0);
}

inline void gearLightOn()
{
	PORTC |= (1<<PINC6);
}

void updateStarter()
{
	if (updateDisplay)
	{
		if (gear == 0)
		{
			if (asms && substate == AMI_SUBSTATE_ENABLE_STARTER)
			{
				enStarter();
				} else {
				disStarter();
			}
			gearLightOn();
			} else {
			gearLightOff();
			disStarter();
		}
	}
}

void setUImodeIndicator() {
	
	LCD_cursor(2, 0);
	
	if (mode == AMI_STATE_SHOW_RELEASE)
	{
		LCD_string("--HOLD----HOLD--");
		return;
	}
	
	if (mode == AMI_STATE_LOCKED || mode == AMI_STATE_SHOW_RESTART)
	{
		LCD_string("----------------");
		return;
	}
	
	if (selected && !confirmed)
	{
		LCD_string("--Yes-----Nooo--");
		return;
	}
	
	if (lockedPosition > MISSION_MAINTENANCE)
	{
		LCD_string("-----AUTO<3-----");
		return;
	}
	
	if (lockedPosition == MISSION_MANUAL)
	{
		LCD_string("-----MANUAL-----");
		return;
	}
	
	if (lockedPosition == MISSION_NONE)
	{
		LCD_string("-->>-------SEL--");
		return;
	}
	
	// if you are here, something went wrong
	LCD_string("CRIES IN SPANISH");
}

void showConfirmationQuestion() {
	LCD_clear();
	LCD_cursor(0, 0);
	LCD_string("                ");
	
	LCD_cursor(1, 0);
	LCD_string(" ARE YOU SAFE?! ");
}

void showRestartScreen() {
	LCD_clear();
	LCD_cursor(0, 0);
	
	if (substate == AMI_SUBSTATE_NORMAL)
	{
		if (current == MISSION_MANUAL && confirmed)
		{
			LCD_string("                ");
		}
		else
		{
			LCD_string("     PLEASE     ");
		}
	}
	
	if (substate == AMI_SUBSTATE_ENABLE_STARTER)
	{
		LCD_string("BIG GREEN BUTTON");
	}
	
	if (substate == AMI_SUBSTATE_CLOSE_SC)
	{
		LCD_string("  CLOSE THE SC  ");
	}
	
	LCD_cursor(1, 0);
	
	if (substate == AMI_SUBSTATE_NORMAL)
	{	
		if (current == MISSION_MANUAL && confirmed)
		{
			LCD_string("    HAVE FUN!   ");
		} else
		{
			LCD_string("   POWERCYCLE   ");
		}
	}
	
	if (substate == AMI_SUBSTATE_ENABLE_STARTER)
	{
		LCD_string("    DO PRESS    ");
	}
	
	if (substate == AMI_SUBSTATE_CLOSE_SC)
	{
		LCD_string("   POR FAVOR    ");
	}
}

//yellow cable function
void cycleHandler() {
	
	uint8_t nextEntry = 0;
	nextEntry = (current + 1) % MISSION_COUNT;
					
	// skip mission none and finished
	// they should not be selectable
	// need this
	if (nextEntry == MISSION_NONE || nextEntry == MISSION_MAINTENANCE)
	{
		nextEntry++;
	}
	
	current = nextEntry;
}

//green cable function
void selectionHandler(){
	
	selected = 1;
	confirmed = 0;
	interruptsSet = 0;
}

void yesHandler(){
	
	mode = AMI_STATE_LOCKED;
	lockedPosition = current;
	selected = 1;
	confirmed = 1;
	interruptsSet = 0;
}

void noHandler(){
	
	selected = 0;
	confirmed = 0;
	interruptsSet = 0;
	lockedPosition = MISSION_NONE;
}

void timerHandler() 
{	
	if (syslogCounter == 5)
	{
		sendSysLog(mode, lockedPosition, byte6, byte7);
		syslogCounter = 0;
	}
	
	if (waitCounter != 100)
	{
		waitCounter++;
		if (waitCounter == 100)
		{
			hasWaitedForPressure = true;
		}
	}
	
	if (displayUpdateCounter == 100)
	{
		displayUpdateCounter = 0;
		updateDisplay = 1;
	}
	
	// was 100 for both
	if (frontPressure < 20 && rearPressure < 20 && mode == AMI_STATE_SHOW_RELEASE && hasWaitedForPressure)
	{
		mode = AMI_STATE_NORMAL;
		interruptsSet = 0;
	}
	
	updateButtonStates();
	
	syslogCounter++;
	displayUpdateCounter++;
}

void enableMessageTimerFromStart() 
{
	initTimerMS(TIMER_TIMEOUT, &timerHandler);
}

uint8_t handleDash(canMsg* msg) {
	// get only gear low byte
	gear = (msg->data[1] - 2);
	
	return RECEIVE_CONTINUE;
}

uint8_t handleASState(canMsg* msg) {
	
	uint8_t amiReceive = msg->data[7] >> 2;
	uint8_t asStateAmiMode = getCSState(amiReceive);
	asState = msg->data[0];
	substate = getCSSubstate(amiReceive);
	
	if ((asStateAmiMode == AMI_STATE_SHOW_RESTART || asStateAmiMode == AMI_STATE_LOCKED || !confirmed) 
		&& mode != AMI_STATE_SHOW_RELEASE)
	{
		interruptsSet = 0;
		mode = asStateAmiMode;
	}
	
	return RECEIVE_CONTINUE;
}

uint8_t handleAssc(canMsg* msg) {
	// get asms state
	//asms = (msg->data[7] >> 7);
	
	return RECEIVE_CONTINUE;
}

uint8_t handlesbSensors(canMsg* msg) 
{
	if (!hasWaitedForPressure)
	{
		return RECEIVE_CONTINUE;
	}
	
	frontPressure = to16bit(msg->data[1], msg->data[0]);
	rearPressure = to16bit(msg->data[3], msg->data[2]);
	
	if (mode == AMI_STATE_NORMAL
		&& (frontPressure > 20 || rearPressure > 20) )
	{
		mode = AMI_STATE_SHOW_RELEASE;
		interruptsSet = 0;
	}
	
	return RECEIVE_CONTINUE;
}

// main function
int main(void){

    // init Starter Output
	DDRC |= (1 << PINC6);
	DDRB |= (1 << PINB0);
	disStarter();
	gearLightOff();
	
	// init CAN test
	initCAN();
	initSysLog();
	
	asStateMessage.id = MSG_ID_ASSTATE;
	asStateMessage.length = 8;
	
	dashOneMessage.id = 0x201;
	dashOneMessage.length = 8;
	
	asscSysLog.id = 0x120;
	asscSysLog.length = 8;
	
	sbSensorMessage.id = 0x14B;
	sbSensorMessage.length = 8;
	
	receiveCANMsg(&asStateMessage, FULL_FILTERING, &handleASState);
	receiveCANMsg(&dashOneMessage, FULL_FILTERING, &handleDash);
	receiveCANMsg(&sbSensorMessage, FULL_FILTERING, &handlesbSensors);
	receiveCANMsg(&asscSysLog, FULL_FILTERING, &handleAssc);
	
	enableMessageTimerFromStart();
	
	// init LCD
	LCD_init();
	initInts();
	
    while (1) 
	{
		if (mode == AMI_STATE_NORMAL)
		{
			if (!selected)
			{
				buttonModeCycle();
			}
			
			if (selected && !confirmed)
			{
				buttonModeConfirm();
			}
		}
		else
		{
			buttonModeOff();
		}
		
		updateStarter();
		updateScreen();
    }
}

