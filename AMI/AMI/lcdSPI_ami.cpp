/*
 * lcd.c
 *
 *	Created: 11.12.2015 17:46:34
 *  Author: Frank
 *	Updated: 13.01.2018
 *	Extended: Stephan
 *	5V EADOGM Display SPI Driver, PINS for ATmega16/32/64M1
 *
 */ 
 
/*#include "lcdSPI_ami.h"


#include <avr/io.h>
#define F_CPU 16000000
#include <util/delay.h>

//select if alternative pins for spi are used or not (only atmega16/32/64M1
#define SPI_ALTERNATIVE_PINS_USED 1

//Module Defines
#define DOGM081 1
#define DOGM162 2
#define DOGM163 3

//Select one from the types above to support your display type
#define LCD_TYPE DOGM163

#if SPI_ALTERNATIVE_PINS_USED

// Display CLK (SPI clock)
#define SCK_BIT		(1<<PIND4)
#define SCK_PORT	PORTD
#define SCK_DDR		DDRD


// Display SI (SPI MOSI)
#define MOSI_BIT	(1<<PIND3)
#define MOSI_PORT	PORTD
#define MOSI_DDR	DDRD


// SS - not connected (Must be set, so that AVR acts as Master!)
#define SS_BIT		(1<<PINC7)
#define SS_PORT		PORTC
#define SS_DDR		DDRC

#else

// Display CLK (SPI clock)
#define SCK_BIT		(1<<PINB7)
#define SCK_PORT	PORTB
#define SCK_DDR		DDRB

// Display SI (SPI MOSI)
#define MOSI_BIT	(1<<PINB1)
#define MOSI_PORT	PORTB
#define MOSI_DDR	DDRB

// SS - not connected (Must be set, so that AVR acts as Master!)
#define SS_BIT		(1<<PIND3)
#define SS_PORT		PORTD
#define SS_DDR		DDRD

#endif

// Display RS "register select"
// use dedicated GPO pin PB0
#define RS_BIT		(1<<PINB3)
#define RS_PORT		PORTB
#define RS_DDR		DDRB

// Display CSB "chip select"
// both defines are tested okay, but need different wiring. When
// use dedicated GPO pin PC1
#define SS0_BIT		(1<<PINB4)
#define SS0_PORT	PORTB
#define SS0_DDR		DDRB

void LCD_command(uint8_t cmd)
{
	SS0_PORT &= ~SS0_BIT;			// CSB low
	RS_PORT &= ~RS_BIT;				//  RS low
	SPDR = cmd;
	while (!(SPSR & (1<<SPIF)));	// wait for SPI send complete
	SS0_PORT |= SS0_BIT;			// CSB high
	_delay_us(26.3);				// see datasheet
}

void LCD_char(char c)
{
	SS0_PORT &= ~SS0_BIT;			// CSB low
	RS_PORT |= RS_BIT;				//  RS high
	SPDR = c;
	while (!(SPSR & (1<<SPIF)));	// wait for SPI send complete
	SS0_PORT |= SS0_BIT;			// CSB high
}

void LCD_string(char *str)
{
	while(*str)
		LCD_char(*str++);
}

void LCD_home()
{
	LCD_command(0x02); // cursor home
	_delay_ms(1);      // see datasheet
}

void LCD_clear()
{
	LCD_command(0x01); // clear
	_delay_ms(1);      // see datasheet
}

// line (first line==0), col (first column==0)
int LCD_cursor(unsigned int line, unsigned int col)
{
	if(LCD_TYPE == DOGM081 && col>7)
		return -1;
	if( (line > LCD_TYPE - 1) || (col>15) ) 
		return -1; // error
	LCD_command(0x80 + 0x10*line + col);	// Set DDRAM address command
	return 0; // OK
}

void LCD_init()
{
	RS_DDR |= RS_BIT;		// output
	RS_PORT |= RS_BIT;		// high
	
	SS0_DDR |= SS0_BIT;		// output
	SS0_PORT |= SS0_BIT;	// high
	
	SCK_DDR |= SCK_BIT;		// output
	SCK_PORT |= SCK_BIT;	// high
	
	MOSI_DDR |= MOSI_BIT;	// output
	MOSI_PORT |= MOSI_BIT;	// high
	
	// SS kann im SPI Master Mode als GPO benutzt werden
	// falls man SS nicht als GPO nutz, ist es SPI *Input* f�r Multi-Master Bus Arbitration)
	//SS_DDR |= SS_BIT;		// output
	//SS_PORT |= SS_BIT;		// high		

	//  enable SPI master, mode 3, clk = 1/64 (1/16 was too fast -> skipping chars)
	if(SPI_ALTERNATIVE_PINS_USED)
		MCUCR |= (1<<SPIPS);
	else
		MCUCR &= ~(1<<SPIPS);

	SPCR = (1<<SPE)|(0<<DORD)|(1<<MSTR)|(1<<CPOL)|(1<<CPHA)|(1<<SPR1)|(0<<SPR0);
	_delay_ms(250);

	#if LCD_TYPE == DOGM081

	// from DOGM163 data sheet, 5V
	LCD_command(0x31);	// 8 bit data length, 2 lines, instruction table 1
	LCD_command(0x1C);	// BS: 1/5, 2 line LCD
	LCD_command(0x51);	// booster on, contrast C5, set C4
	LCD_command(0x6A);	// set voltage follower and gain
	LCD_command(0x74);	// set contrast C3, C2, C1
	LCD_command(0x30);	// instruction table 0
	LCD_command(0x0C);	// display on, (cursor on: +2, blinking on: +1)
	LCD_clear();		// display clear, cursor home
	LCD_command(0x06);	// data entry mode: auto increment

	#endif

	#if LCD_TYPE == DOGM162

	// from DOGM163 data sheet, 5V
	LCD_command(0x39);	// 8 bit data length, 2 lines, instruction table 1
	LCD_command(0x1C);	// BS: 1/5, 2 line LCD
	LCD_command(0x52);	// booster on, contrast C5, set C4
	LCD_command(0x69);	// set voltage follower and gain
	LCD_command(0x74);	// set contrast C3, C2, C1
	LCD_command(0x38);	// instruction table 0
	LCD_command(0x0C);	// display on, (cursor on: +2, blinking on: +1)
	LCD_clear();		// display clear, cursor home
	LCD_command(0x06);	// data entry mode: auto increment

	#endif

	#if LCD_TYPE == DOGM163

	// from DOGM163 data sheet, 5V
	LCD_command(0x39);	// 8 bit data length, 2 lines, instruction table 1
	LCD_command(0x1D);	// BS: 1/5, 2 line LCD
	LCD_command(0x50);	// booster on, contrast C5, set C4
	LCD_command(0x6C);	// set voltage follower and gain
	LCD_command(0x78);	// set contrast C3, C2, C1
	LCD_command(0x38);	// instruction table 0
	LCD_command(0x0C);	// display on, (cursor on: +2, blinking on: +1)
	LCD_clear();		// display clear, cursor home
	LCD_command(0x06);	// data entry mode: auto increment

	#endif



	/*
	// from DOGM162 data sheet, 3.3V
	LCD_command(0x39);	// 8 bit data length, 2 lines, instruction table 1
	LCD_command(0x15);	// BS: 1/5, 2 line LCD
	LCD_command(0x55);	// booster on, contrast C5, set C4
	LCD_command(0x6E);	// set voltage follower and gain
	LCD_command(0x72);	// set contrast C3, C2, C1
	LCD_command(0x38);	// instruction table 0
	LCD_command(0x0c);	// display on, (cursor on: +2, blinking on: +1)
	LCD_clear();		// display clear, cursor home
	LCD_command(0x06);	// data entry mode: auto increment
	*/
//}
