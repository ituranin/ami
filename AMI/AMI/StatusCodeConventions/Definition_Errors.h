/*
 * ErrorCodes.h
 *
 * Created: 18.01.2018 13:27:09
 *  Author: Stephan Dunkel
 */ 


#ifndef DEFINITION_ERRORS_H
#define DEFINITION_ERRORS_H

//error type
#define ERR_TYPE_UNKNOWN	255
#define ERR_TYPE_SYSTEM		1
#define ERR_TYPE_SENSOR		2
#define ERR_TYPE_CONTROL	3
#define ERR_TYPE_CAN		4

//error source:
#define ERR_SRC_UNKNOWN		255

//general err srcs
#define ERR_SRC_DEVID		1	

//connected analog devices
#define ERR_SRC_BSPD		40
#define ERR_SRC_EBS			41

//communication err srcs
#define ERR_SRC_RX_ASSTATE	50
#define ERR_SRC_RX_TARGVAL	51

#define ERR_SRC_SENSOR_DEVITAION	90

//specific sensors
#define ERR_SRC_HSP1		101	//hydraulic system pressure 1
#define ERR_SRC_HSP2		102	//hydraulic system pressure 2
#define ERR_SRC_HST			103	//hydraulic system temperature
#define ERR_SRC_BPF			104	//brake pressure front
#define ERR_SRC_BPR			105	//brake pressure rear
#define ERR_SRC_HBPF		106	//hydraulic brake pressure front
#define ERR_SRC_HBPR		107	//hydraulic brake pressure rear
#define ERR_SRC_SCPL		108	//steering cylinder pressure left
#define ERR_SRC_SCPR		109	//steering cylinder pressure right
#define ERR_SRC_SAS			110	//steering angle sensor
#define ERR_SRC_STS			111	//steering travel sensor
#define ERR_SRC_CLP			112	//clutch pressure

#define ERR_SRC_TPS1		113	//throttle position
#define	ERR_SRC_TPS2		114
#define	ERR_SRC_APPS1		115	//pedal position
#define	ERR_SRC_APPS2		116

//specific controls
#define ERR_SRC_HYDPUMP		201	//hydraulic pump
#define ERR_SRC_STVALVE		202	//steering valve (servo)
#define ERR_SRC_BRVALVE		203	//brake valve (servo)
#define ERR_SRC_CLVALVE		204	//cl valve (solenoid valve/servo)
#define ERR_SRC_ETCSERVO	205 //etc servo


// termination reasons (State Machine)
#define TERMINATION_NONE					0
#define TERMINATION_SHUTDOWN_CIRCUIT_OPEN	1
#define TERMINATION_ASMS_OFF				2
#define TERMINATION_WRONG_VALUE				3

#define ERR_SET			4		
#define ERR_CLEARED		5


#endif /* DEFINITION_ERRORS_H */