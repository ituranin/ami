# Status Code Konventionen

Diese Bilbiothek stellt Headerfiles mit Status Codes, Standard Message IDs und anderen Konfigurationsdateien bereit, um die einheitliche Verwendung der Codes und Interpretation dieser in allen Programmen zu ermöglichen und zu updaten. 

## Einbindung in eigenes AVR Studio Projekt via Submodule

### Einrichtung

Um die Treiberbibliothek zu verwenden, muss die AVR Driver Lib in das Projekt hinzugefügt werden. Dazu in den Projektordner navigieren, in dem die main.cpp liegt, die git bash öffnen und den folgenden Befehl ausführen. 

    $ git submodule add -b master https://gitlab.htw-motorsport.de/elektrik/StatusCodeConventions.git  

Anschließend in AVR Studio im Solution Explorer auf das Symbol 
    
    show all files 

klicken und dann mit Rechtsklick auf den weißen Ordner 

    StatusCodeConventions -> include in Project

auswählen.

Im Quellcode können die Treiber wie folgt eingebunden werden

    #include "StatusCodeConventions/example.h"

### Update der Status Code Bibliothek im Projekt

Dazu in den Unterordner "StatusCodeConventions" im Projektordner navigieren, die git bash öffnen und den folgenden Befehl ausführen:

    $ git pull
    
### Eingebundene Submodule laden

Beim checkout eines Repositories mit bereits eingebundenen Modulen, werden die besagten Module nicht automatisch mitgeladen. Der folgende Befehl muss nach dem Checkout ausgeführt werden.

    $ git submodule update --recursive --init