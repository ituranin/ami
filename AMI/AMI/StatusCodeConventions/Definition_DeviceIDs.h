/*
 * DefinitionDeviceIDs.h
 *
 * Created: 24.01.2018 22:23:56
 *  Author: Stephan Dunkel
 */ 

#ifndef DEFINITION_DEVICE_IDS_H_
#define DEFINITION_DEVICE_IDS_H_

#define DEV_ID_ASSC				0x0
#define DEV_ID_ASCPU			0x1
#define DEV_ID_ASEBS			0x2
#define DEV_ID_ASSBC			0x3
#define DEV_ID_ASSAC			0x4
#define DEV_ID_ASCAC			0x5
#define DEV_ID_SWC				0x6
#define DEV_ID_APPS_DECODER		0x7
#define DEV_ID_ETC				0x8
#define DEV_ID_AMI				0x9
#define DEV_ID_ASSI_REAR		0xA
#define DEV_ID_ASSI_LEFT		0xB
#define DEV_ID_ASSI_RIGHT		0xC
#define DEV_ID_ASHSC			0xD
//#define DEV_ID_		0xE
//#define DEV_ID_		0xF

#endif /* DEFINITION_DEVICE_IDS_H_ */