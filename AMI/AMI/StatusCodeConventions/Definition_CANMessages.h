/*
 * Definition_CANMessages.h
 *
 * Created: 18.01.2018 13:37:17
 *  Author: Stephan
 */ 


#ifndef DEFINITION_CANMESSAGES_H_
#define DEFINITION_CANMESSAGES_H_

//Autonomous System Controller Message Defines

//Message Lengths
#define MSG_DLC_ASSTATE			3
#define MSG_DLC_SYSLOG			4
#define MSG_DLC_ERROR			2
#define MSG_DLC_CONTROL			8
#define MSG_DLC_CONTROL_PID		8
#define MSG_DLC_TARG_VAL		2
#define MSG_DLC_SENSOR_DATA		8
#define MSG_DLC_SENSOR_VOLTAGES	8
#define MSG_DLC_SENSOR_RAW_DATA 8

// calibration routine
#define MSG_DLC_ACKNOWLEDGEMENT 8
#define MSG_DLC_BEGIN_COM       8
#define MSG_DLC_COMMAND         8
#define MSG_DLC_DATA            8
#define MSG_DLC_CERROR          8
#define MSG_DLC_FINISH          8

//Message Base ID -> to get id: BaseID + devID
#define MSG_BASE_ID_SYSLOG			0x120
#define MSG_BASE_ID_TARG_VAL		0x130
#define MSG_BASE_ID_CONTROL			0x140
#define MSG_BASE_ID_SENSOR_DATA		0x150
#define MSG_BASE_ID_SENSOR_VOLTAGES 0x160
#define MSG_BASE_ID_SENSOR_RAW_DATA 0x170
#define MSG_BASE_ID_ERROR			0x180
#define MSG_BASE_ID_CONTROL_PID		0x190

//single MSG IDs
#define MSG_ID_ASSTATE				0x110

//single MSG IDs for calibration
#define MSG_ID_ACKNOWLEDGEMENT 0x00A
#define MSG_ID_BEGIN_COM       0x00B
#define MSG_ID_COMMAND         0x00C
#define MSG_ID_DATA            0x00D
#define MSG_ID_CERROR          0x00E
#define MSG_ID_FINISH          0x00F

#endif /* DEFINITION_CANMESSAGES_H_ */