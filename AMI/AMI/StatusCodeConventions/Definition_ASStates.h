/*
 * Definition_ASStates.h
 *
 * Created: 18.01.2018 13:37:56
 *  Author: HP
 */ 


#ifndef DEFINITION_ASSTATES_H_
#define DEFINITION_ASSTATES_H_

//////////////////////////////////////////////////////////////////////////
//AS States
#define AS_STATE_OFF		0
#define AS_STATE_READY		1
#define AS_STATE_DRIVING	2
#define AS_STATE_FINISHED	3
#define AS_STATE_EBS		4
#define AS_STATE_MANUAL		5
#define AS_STATE_LIGHT      6   // extra light mode state

//ETC States
#define ETC_STATE_OFF			0
#define ETC_STATE_AUTONOMOUS	1
#define	ETC_STATE_MANUAL		2

//BAC-EBS States

#define EBS_STATE_UNAVAILABLE   0
#define EBS_STATE_ARMED         1
#define EBS_STATE_ACTIVATED     2

//BAC_SBC States

#define SB_STATE_UNAVAILABLE    0
#define SB_STATE_ENGAGED        1
#define SB_STATE_AVAILABLE      2

//SAC States

#define SA_STATE_UNAVAILABLE    0
#define SA_STATE_AVAILABLE      1

//HSC States

#define HS_STATE_UNAVAILABLE    0
#define HS_STATE_AVAILABLE      1

//CAC States

#define CA_STATE_UNAVAILABLE    0
#define CA_STATE_ENGAGED        1
#define CA_STATE_AVAILABLE      2

//MCU States
#define MCU_STATE_PASSIV        0
#define MCU_STATE_LIGHT         1

//CPU States
#define CPU_STATE_OFF           0
#define CPU_STATE_READY         1
#define CPU_STATE_ACTIVE        2
#define CPU_STATE_FINISHED      3

//AMI States
#define AMI_STATE_NORMAL        0
#define AMI_STATE_SHOW_RESTART  1
#define AMI_STATE_SHOW_RELEASE  2
#define AMI_STATE_LOCKED        3

// AMI SUBSTATES
#define MISSION_NONE            0
#define MISSION_MANUAL          1
#define MISSION_MAINTENANCE     2
#define MISSION_ACCELERATION    3
#define MISSION_SKIDPAD         4
#define MISSION_AUTOCROSS       5
#define MISSION_TRACKDRIVE      6
#define MISSION_EBSTEST         7
#define MISSION_INSPECTION      8

// ASSI States
#define ASSI_STATE_OFF          0
#define ASSI_STATE_YELLOW_CONST 1
#define ASSI_STATE_YELLOW_FLASH 2
#define ASSI_STATE_BLUE_CONST   3
#define ASSI_STATE_BLUE_FLASH   4


#endif /* DEFINITION_ASSTATES_H_ */