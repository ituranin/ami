/*
 * Definition_Sensors.h
 *
 * Created: 25.01.2018 14:09:40
 *  Author: Stephan Dunkel	
 */ 


#ifndef DEFINITION_SENSORS_H_
#define DEFINITION_SENSORS_H_

//Accelerator Pedal Position Sensors
#define APPS_MIN			0
#define APPS_MAX			1050
#define APPS1_OFFSET		0
#define APPS2_OFFSET		0
#define APPS1_GRADIENT		1
#define APPS2_GRADIENT		2
#define APPS1_DEFAULT		001
#define APPS2_DEFAULT		001
#define APPS1_MAX_ERRORS	10
#define APPS2_MAX_ERRORS	10
#define APPS_DEV_MAX_ERRORS 10

//Throttle Position Sensors
#define TPS_MIN				0
#define TPS_MAX				1050
#define TPS1_OFFSET			0
#define TPS2_OFFSET			0
#define TPS1_GRADIENT		1
#define TPS2_GRADIENT		2
#define TPS1_DEFAULT		001
#define TPS2_DEFAULT		001
#define TPS1_MAX_ERRORS		10
#define TPS2_MAX_ERRORS		10
#define TPS_DEV_MAX_ERRORS	10

#endif /* DEFINITION_SENSORS_H_ */