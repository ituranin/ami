/*
 * Definition_Storage.h
 *
 * Created: 31.01.2018 23:25:46
 *  Author: Igor
 */ 


#ifndef DEFINITION_STORAGE_H_
#define DEFINITION_STORAGE_H_


//Data Instructinos
#define STORAGE_INSTRUCTION_OVERRIDE	0
#define STORAGE_INSTRUCTION_ADD         1
#define STORAGE_INSTRUCTION_SUB			2

//Command Instructions
#define STORAGE_INSTRUCTION_WRITE       3
#define STORAGE_INSTRUCTION_WRITEALL    4
#define STORAGE_INSTRUCTION_WRITEONLINE 5
#define STORAGE_INSTRUCTION_READ		6	
//#define STORAGE_INSTRUCTION_READ_ALL	7	
#define STORAGE_INSTRUCTION_HARDRESET	8	//Reset EEPROM to initial programmed values


//Storage Capacity -> Maximum is 256!
//Choose highest field definition Value + 1!
#define STORAGE_CAPACITY									256

//Field - Definitions

//General
#define ST_ID_DEBUGMODE										0
#define ST_ID_ERR_VAL_EN									1

//PID Control starting at 10
#define ST_ID_P												10
#define ST_ID_I												11
#define ST_ID_D												12
#define ST_ID_MAX_CONTROLL_DEVIATION						13
#define ST_ID_PIDTHRESHOLD									14

//Servo starting at 20
#define ST_ID_SERVO_MIN										20
#define ST_ID_SERVO_MAX										21
#define ST_ID_SERVO_OFFSET									22
#define ST_ID_SERVO_DEFAULT									23

#define ST_ID_IDLE											25

//Valve: starting at 30
#define ST_ID_VALVE_DELAYTIME								30
#define ST_ID_VALVE_OPENING_TIME							31

//Sensors General starting at 40
#define ST_ID_SENSOR_MAX_DEVIATION							40

//Sensor1 starting at 50
#define ST_ID_SENSOR1_MIN									50									
#define ST_ID_SENSOR1_MAX									51
#define ST_ID_SENSOR1_OFFSET								52
#define ST_ID_SENSOR1_GRADIENT_MUL							53
#define ST_ID_SENSOR1_GRADIENT_SHIFT						54		//right shifts: 1/2^n
#define ST_ID_SENSOR1_DEFAULT								55

//Sensor2 starting at 60
#define ST_ID_SENSOR2_MIN									60		//min SensorValue
#define ST_ID_SENSOR2_MAX									61
#define ST_ID_SENSOR2_OFFSET								62
#define ST_ID_SENSOR2_GRADIENT_MUL							63
#define ST_ID_SENSOR2_GRADIENT_SHIFT						64
#define ST_ID_SENSOR2_DEFAULT								65

//Sensor3 starting at 70
#define ST_ID_SENSOR3_MIN									70
#define ST_ID_SENSOR3_MAX									71
#define ST_ID_SENSOR3_OFFSET								72
#define ST_ID_SENSOR3_GRADIENT_MUL							73
#define ST_ID_SENSOR3_GRADIENT_SHIFT						74
#define ST_ID_SENSOR3_DEFAULT								75

//Sensor4 starting at 80
#define ST_ID_SENSOR4_MIN									80
#define ST_ID_SENSOR4_MAX									81
#define ST_ID_SENSOR4_OFFSET								82
#define ST_ID_SENSOR4_GRADIENT_MUL							83
#define ST_ID_SENSOR4_GRADIENT_SHIFT						84
#define ST_ID_SENSOR4_DEFAULT								85


//CAN Communication General
#define ST_ID_CAN_TIMEOUT									90

//CAN Communication IDs	

#endif /* DEFINITION_STORAGE_H_ */