/*
 * Definition_ControllerStates.h
 *
 * Created: 18.01.2018 14:16:22
 *  Author: HP
 */ 

#ifndef DEFINITION_CONTROLLERMODES_H
#define DEFINITION_CONTROLLERMODES_H

#define CM_MANUAL		0
#define CM_AUTONOMOUS	1

#endif



#ifndef DEFINITION_CONTROLLERSTATES_H_
#define DEFINITION_CONTROLLERSTATES_H_

#define CS_INIT				5
#define CS_READY			2
#define CS_ACTIVE			4
#define CS_FAILURE			1
#define CS_SAFESTATE		6
#define CS_TEST				3
#define CS_CALIBRATION		7

//rules state definitions:

//Service Brake
#define CRS_SB_DISENGAGED	0
#define CRS_SB_ENGAGED		1
#define CRS_SB_AVAILABLE	2

//EBS
#define CRS_EBS_UNAVAILABLE 0
#define CRS_EBS_ARMED		1
#define CRS_EBS_TRIGGERED	2

//Steering
#define CRS_ST_OFF			0
#define CRS_ST_ACTIVE		1

//Clutch
#define CRS_CL_OFF			0
#define CRS_CL_ACTIVE		1

//Hydraulic Pump/System
#define CRS_HSC_UNAVAILABLE 0
#define CRS_HSC_OFF			1
#define CRS_HSC_ON			2

//ETC
#define CRS_ETC_UNAVAILABLE		0
#define CRS_ETC_ACTIVE			1

#endif /* DEFINITION_CONTROLLERSTATES_H_ */