/*
 * Definition_Actuators.h
 *
 * Created: 25.01.2018 22:53:48
 *  Author: Stephan Dunkel
 */ 


#ifndef DEFINITION_ACTUATORS_H_
#define DEFINITION_ACTUATORS_H_

//ETC Servo
#define ETS_OUTPUT		1		//Servo B
#define ETS_DEFAULT		0
#define ETS_MIN			0
#define ETS_MAX			1000		//throttle target position percentage *10
#define ETS_TOLERANCE	100

#define SERVO_TARG_MAX_ERRORS	10
#define SERVO_MAX_ERRORS		10


#endif /* DEFINITION_ACTUATORS_H_ */