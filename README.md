# AMI

Der Autonomous Mission Indicator ist für die Missionsaufwahl zuständig. Das Interface besteht aus zwei Buttons und einem 3-Zeilen LCD.

Dem Nutzer werden nach der Auswahl Informationen zum aktuellen Status angezeigt. Wenn benötigt, wird der Nutzer gebeten das System zu bedienen. So muss zum Beispiel der Starter aus Sicherheitsgründen manuell betätigt werden.

Im Fall eines Emergency-Brake oder eines erfolgreichen Abschlusses wird der Bremsdruck angezeigt und die Button-Funktion umprogrammiert. Der Nutzer wird zum Halten beider Knöpfe aufgefordert, um den Systemdruck zu lösen. Die Bremsdruckanzeige gibt das entsprechende Feedback für das Ende des Vorganges.

Die Nutzerfreundlichkeit konnte am FSG Event 2019 durch Track-Marshalls ohne Vorerfahrung bestätigt werden.

## UI-Beispiele
### Missionsauswahl
![Mission-Selection AMI 1.0](mission_select.png)
 
### Lösung des EBS
![Pressure-Release AMI 1.0](pressure_release.png)

Der AMI ist rechts am Sideplate am BRC positioniert. Links steht der Bremsdruck für die Hinterräder, rechts für die Vorderräder.

## AMI 2.0

Eine neue Version befindet sich in der Entwicklung, der Code ist allerdings noch nicht fertig.
Hier ist ein Beispiel des neuen Interfaces:
![Mission-Selection AMI 2.0](pcb_ami2.jpg)
